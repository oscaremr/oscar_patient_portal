<%@page import="org.oscarehr.myoscar.util.MiscUtils"%>
<%@page import="org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo"%>
<%@page import="org.oscarehr.myoscar.util.WebUtils"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.BookAppointment"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager"%>
<%@page import="java.util.Calendar"%>
<%
	Calendar startTime=(Calendar)session.getAttribute("selectTimeSlot");
	String providerId=(String)session.getAttribute("providerId");
	Long appointmentTypeId=(Long)session.getAttribute("appointmentTypeId");
	String notesComments=(String)session.getAttribute("appointmentNotes");
	
	try
	{
		MyOscarLoggedInInfo loggedInInfo=MyOscarLoggedInInfo.getLoggedInInfo(session);
		Integer appointmentId=BookAppointment.bookAppointment(loggedInInfo, startTime, providerId, appointmentTypeId, notesComments);
		
		if (appointmentId!=null)
		{
	response.sendRedirect("book_appointment_result_success.jsp");
	return;
		}
	}
	catch (Exception e)
	{
		MiscUtils.getLogger().error("Unexpected error", e);
	}
	
	response.sendRedirect("book_appointment_result_failure.jsp");
%>