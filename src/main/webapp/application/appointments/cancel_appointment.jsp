<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.PreviousAppointments"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarProviderManager"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%
	Integer appointmentId=Integer.parseInt(request.getParameter("appointmentId"));

	PreviousAppointments.cancelAppointment(MyOscarLoggedInInfo.getLoggedInInfo(session), appointmentId);
%>

<%@include file="/resources/html_top.jspf"%>

<div style="text-align:center">
	<h2 style="color:#00cc00">
		<%=LocaleUtils.getMessage(request, "AppointmentCancelSuccess") %>
	</h2>
	<input type="button" value="<%=LocaleUtils.getMessage(request, "Continue")%>" onclick="document.location.href='manage_appointments.jsp'" />
</div>


<%@include file="/resources/html_bottom.jspf"%>
