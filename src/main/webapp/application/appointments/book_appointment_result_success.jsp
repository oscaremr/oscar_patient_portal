<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@include file="/resources/html_top.jspf"%>

<%
	Calendar cal=(Calendar)session.getAttribute("selectTimeSlot");
	SimpleDateFormat sdf=new SimpleDateFormat("EEEE, yyyy-MM-dd, hh:mm a", request.getLocale());
	
	session.removeAttribute("appointmentTypeId");
	session.removeAttribute("providerId");
	session.removeAttribute("dateSelectedString");
	session.removeAttribute("appointmentNotes");
	session.removeAttribute("selectTimeSlot");
	session.removeAttribute("nextAvailableAppointment");
%>
<div style="text-align:center">
	<h2 style="color:#00cc00">
		<%=LocaleUtils.getMessage(request, "AppointmentBookingSuccess") %>
	</h2>
	<div>
		<span style="font-weight:bold">
			<%=LocaleUtils.getMessage(request, "Date")%>
		</span>
		: <%=sdf.format(cal.getTime())%>
	</div>
	<input type="button" value="<%=LocaleUtils.getMessage(request, "Continue")%>" onclick="document.location.href='<%=request.getContextPath()%>/customisations'" />
</div>

<%@include file="/resources/html_bottom.jspf"%>
