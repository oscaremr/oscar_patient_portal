<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.ViewWeekAppointmentSlots"%>
<%@include file="/resources/html_top.jspf"%>

<%
	Boolean nextAvailableAppointment=(Boolean)session.getAttribute("nextAvailableAppointment");
	String dateSelectedString=(String)session.getAttribute("dateSelectedString");
	String providerId=(String)session.getAttribute("providerId");
%>

<h3><%=LocaleUtils.getMessage(request, "AvailableAppointments")%></h3>
<hr />

<%=LocaleUtils.getMessage(request, "AvailabilityFor")%> : <%=ViewWeekAppointmentSlots.getProviderFullNameHtmlEscaped(providerId)%>
<br />
<%=LocaleUtils.getMessage(request, "WeekOf")%> : <%=dateSelectedString%>
<br /><br />
<%
	ArrayList<String> weekDays=ViewWeekAppointmentSlots.get1WeekDatesIsoFormat(dateSelectedString);
	for (String day : weekDays)
	{
		%>
			<jsp:include page="view_day_appointment_slots.jsp">
				<jsp:param name="date" value="<%=day%>" />
			</jsp:include>
		<%	
	}
%>
<br />
<div style="text-align:center">
	<input type="button" value="<%=LocaleUtils.getMessage(request, "ShowNextAvailableAppointments")%>" onclick="document.location.href='find_next_available_appointment_action.jsp'" />
	&nbsp;&nbsp;
	<input type="button" value="<%=LocaleUtils.getMessage(request, "Back")%>" onclick="history.go(-1)" />
</div>


<%@include file="/resources/html_bottom.jspf"%>
