<%@page import="org.oscarehr.myoscar.util.WebUtils"%>
<%@page import="org.apache.commons.lang.time.DateFormatUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.SelectProviderDate"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarProviderManager"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%
	String providerId=request.getParameter("providerId");
	// must double check posted item as clients can alter posted data.
	if (OscarProviderManager.isProviderInAllowedList(providerId))
	{
		session.setAttribute("providerId", providerId);
	}
	else
	{
		throw(new IllegalStateException("some one doing a dodgy post of a providerNo we disallow? like a manual post or something? providerId="+providerId));
	}
	
	Long appointmentTypeId=Long.parseLong(request.getParameter("appointmentTypeId"));
	session.setAttribute("appointmentTypeId", appointmentTypeId);
	
	String dateSelected=request.getParameter("dateSelected");
	session.setAttribute("dateSelectedString", dateSelected);
	
	boolean nextAvailableAppointment=WebUtils.isChecked(request, "nextAvailableAppointment");
	session.setAttribute("nextAvailableAppointment", nextAvailableAppointment);

	String appointmentNotes=StringUtils.trimToNull(request.getParameter("appointmentNotes"));
	session.setAttribute("appointmentNotes", appointmentNotes);
	
	if (nextAvailableAppointment)
	{
		GregorianCalendar cal=new GregorianCalendar();
		cal.add(GregorianCalendar.DAY_OF_YEAR, -1);
		cal.getTimeInMillis();
		session.setAttribute("dateSelectedString", DateFormatUtils.ISO_DATE_FORMAT.format(cal));
		response.sendRedirect("find_next_available_appointment_action.jsp");
		return;
	}

	response.sendRedirect("view_week_appointment_slots.jsp");		
%>