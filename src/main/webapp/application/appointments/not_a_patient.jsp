<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@include file="/resources/html_top.jspf"%>

<%=LocaleUtils.getMessage(request, "NotCurrentlyPatient")%>
<input type="button" value="<%=LocaleUtils.getMessage(request, "Back")%>" onclick="history.go(-1)" />

<%@include file="/resources/html_bottom.jspf"%>
