<%@page import="org.oscarehr.oscar_patient_portal.web.SelectProviderDate"%>
<%
	String dateSelectedString=(String)session.getAttribute("dateSelectedString");
	String providerId=(String)session.getAttribute("providerId");
	Long appointmentTypeId=(Long)session.getAttribute("appointmentTypeId");

	String dateSelected=SelectProviderDate.getNextAvailableAppointmentDateString(providerId, appointmentTypeId, dateSelectedString);
	if (dateSelected==null)
	{
		response.sendRedirect("no_appointments_available.jsp");
		return;
	}
	else
	{
		session.setAttribute("dateSelectedString", dateSelected);			
	}
	
	response.sendRedirect("view_week_appointment_slots.jsp");
%>