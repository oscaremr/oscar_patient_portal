<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@include file="/resources/html_top.jspf"%>

<div style="text-align:center">
	<h2 style="color:#cc0000">
		<%=LocaleUtils.getMessage(request, "AppointmentBookingFailure") %>
	</h2>
	<br />
	<input type="button" value="<%=LocaleUtils.getMessage(request, "Back")%>" onclick="history.go(-1)" />
</div>

<%@include file="/resources/html_bottom.jspf"%>
