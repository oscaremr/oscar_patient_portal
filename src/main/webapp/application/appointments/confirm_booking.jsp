<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.AppointmentType"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.commons.lang.time.DateFormatUtils"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.ViewWeekAppointmentSlots"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@include file="/resources/html_top.jspf"%>

<%
	long timeMs=Long.parseLong(request.getParameter("dateTimeMs"));
	Calendar cal=new GregorianCalendar();
	cal.setTimeInMillis(timeMs);
	session.setAttribute("selectTimeSlot", cal);
	SimpleDateFormat sdf=new SimpleDateFormat("EEEE, yyyy-MM-dd, hh:mm a", request.getLocale());
	
	String providerId=(String)session.getAttribute("providerId");
	Long appointmentTypeId=(Long)session.getAttribute("appointmentTypeId");
	AppointmentType appointmentType=OscarScheduleManager.getAppointmentType(appointmentTypeId);
	String notesComments=(String)session.getAttribute("appointmentNotes");
%>

<h3><%=LocaleUtils.getMessage(request, "ReviewAppointmentDetails")%></h3>
<hr />

<table class="genericBorderedTable">
	<tr>
		<td class="lightBlueBackground" style="font-weight:bold"><%=LocaleUtils.getMessage(request, "Provider")%></td>
		<td><%=ViewWeekAppointmentSlots.getProviderFullNameHtmlEscaped(providerId)%></td>
	</tr>
	<tr>
		<td class="lightBlueBackground" style="font-weight:bold"><%=LocaleUtils.getMessage(request, "ReasonForAppointment")%></td>
		<td><%=appointmentType.getName()%></td>
	</tr>
	<tr>
		<td class="lightBlueBackground" style="font-weight:bold"><%=LocaleUtils.getMessage(request, "NotesCommentsAboutAppointment")%></td>
		<td><%=StringEscapeUtils.escapeHtml(notesComments==null?"":notesComments)%></td>
	</tr>
	<tr>
		<td class="lightBlueBackground" style="font-weight:bold"><%=LocaleUtils.getMessage(request, "Date")%></td>
		<td><%=sdf.format(cal.getTime())%></td>
	</tr>
</table>
<br />
<div style="text-align:center">
	<input type="button" value="<%=LocaleUtils.getMessage(request, "BookThisAppointment")%>" onclick="document.location.href='confirm_booking_action.jsp'" />
	&nbsp;&nbsp;
	<input type="button" value="<%=LocaleUtils.getMessage(request, "Back")%>" onclick="history.go(-1)" />
</div>


<%@include file="/resources/html_bottom.jspf"%>
