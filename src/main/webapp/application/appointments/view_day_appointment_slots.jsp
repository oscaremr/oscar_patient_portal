<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.ViewDayAppointmentSlots"%>
<%
	String dateString=request.getParameter("date");
	String dateSelectedString=(String)session.getAttribute("dateSelectedString");
	boolean highlightDay=dateString.equals(dateSelectedString);
	Calendar dateCalendar=ViewDayAppointmentSlots.getCalendar(dateString);

	String providerId=(String)session.getAttribute("providerId");
	Long appointmentTypeId=(Long)session.getAttribute("appointmentTypeId");

	
	String backgroundColour="white";
	if (highlightDay) backgroundColour="yellow";
%>
<div style="display:inline-block;text-align:center;padding:.5em;vertical-align:top;background-color:<%=backgroundColour%>">
	<div class="lightBlueBackground" style="padding-left:1em; padding-right:1em">
		<%=dateString%>
		<br />
		<%=ViewDayAppointmentSlots.getDayOfWeekString(request.getLocale(), dateCalendar)%>
	</div>
	<%
		List<Calendar> availableTimes=ViewDayAppointmentSlots.getTimeSlots(providerId, appointmentTypeId, dateCalendar);
		
		if (availableTimes.size()==0)
		{
			%>
				<%=LocaleUtils.getMessage(request, "NotAvailable")%>
			<%				
		}
		else
		{
			for (Calendar timeSlotStart : availableTimes)
			{
				String timeDisplay=ViewDayAppointmentSlots.getTimeDisplayHtmlEscaped(request.getLocale(), timeSlotStart);
				%>
					<button style="color:green;width:100%" onclick="document.location.href='confirm_booking.jsp?dateTimeMs=<%=timeSlotStart.getTimeInMillis()%>'"><%=timeDisplay%></button>
					<br />
				<%
			}
		}
	%>
</div>