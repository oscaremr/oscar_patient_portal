<%@page import="org.oscarehr.myoscar.util.LocaleUtils"%>
<%@page import="org.oscarehr.myoscar_server.ws.PersonTransfer2"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.AppointmentType"%>
<%@page import="org.oscarehr.oscar_patient_portal.manager.OscarDemographicManager"%>
<%@page import="org.oscarehr.ws.DemographicTransfer"%>
<%@page import="org.oscarehr.ws.AppointmentTransfer"%>
<%@page import="org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.PreviousAppointments"%>
<%@page import="org.oscarehr.myoscar.util.WebUtils"%>
<%@page import="org.oscarehr.ws.ProviderTransfer"%>
<%@page import="org.oscarehr.oscar_patient_portal.web.SelectProviderDate"%>

<%
	// double check they are a patient first.
	MyOscarLoggedInInfo loggedInInfo=MyOscarLoggedInInfo.getLoggedInInfo(session);
	PersonTransfer2 myOscarPersonTransfer = loggedInInfo.getLoggedInPerson();
	DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByMyOscarUserName(myOscarPersonTransfer.getUserName());
	if (oscarDemographic==null)
	{
		response.sendRedirect("not_a_patient.jsp");
		return;
	}
%>

<%@include file="/resources/html_top.jspf"%>

<fieldset class="yellowFieldSet">
	<legend><%=LocaleUtils.getMessage(request, "BookAnAppointment")%></legend>

	<script type="text/javascript">
		function checkForm()
		{
			var providerId=$('#providerId').val();
			if (providerId == null || providerId=='')
			{
				alert("<%=LocaleUtils.getMessage(request, "ProviderRequired")%>");
				return(false);
			}
	
			var appointmentType=$('form input[name="appointmentTypeId"]:checked');
			if (appointmentType == null || appointmentType.val()==null || appointmentType.val=='')
			{
				alert("<%=LocaleUtils.getMessage(request, "AppointmentReasonRequired")%>");
				return(false);
			}
	
			var nextAvailableAppointment=document.getElementsByName('nextAvailableAppointment')[0];
			if (!nextAvailableAppointment.checked)
			{
				var dateSelected=$('#dateSelected').val();
				if (dateSelected == null || dateSelected=='')
				{
					alert("<%=LocaleUtils.getMessage(request, "DateRequired")%>");
					return(false);
				}
			}
			
			return(true);
		}
	</script>
	<form action="select_provider_date_action.jsp" onsubmit="return(checkForm())">
		<div style="display:inline-block; text-align:center">
			<table class="genericBorderedTable" style="background-color:white"> 
				<tr>
					<th>
						<%=LocaleUtils.getMessage(request, "Provider")%>
					</th>
					<th>
						<%=LocaleUtils.getMessage(request, "ReasonForAppointment")%>
					</th>
					<th>
						<%=LocaleUtils.getMessage(request, "Date")%>
					</th>
				</tr>
				
				<tr>
					<td style="vertical-align:top">
						<br />
						<%
							String previouslySelectedProviderId=(String)session.getAttribute("providerId");
											List<ProviderTransfer> providers=SelectProviderDate.getCircleOfCareProviders(loggedInInfo);

											if (providers.size()>0)
											{
						%>
									<select size="10" id="providerId" name="providerId">
										<%
											for (ProviderTransfer providerTransfer : SelectProviderDate.getCircleOfCareProviders(loggedInInfo))
																		{
																			String providerId=providerTransfer.getProviderNo();
																			String selectedString=WebUtils.getSelectedString(providerId.equals(previouslySelectedProviderId));
										%>
												<option value="<%=providerId%>" <%=selectedString%>><%=SelectProviderDate.getProviderFullNameHtmlEscaped(providerTransfer)%></option>
											<%
												}
											%>
									</select>											
								<%
																				}
																								else
																								{
																			%>
									<%=LocaleUtils.getMessage(request, "ProviderNotAcceptingOnlineBookings")%>
								<%
									}
								%>
					</td>
	
					<td style="text-align:left;vertical-align:top">
						<br />
						<%
							Long previouslySelectedAppointmentTypeId=(Long)session.getAttribute("appointmentTypeId");
											for (AppointmentType appointmentType : SelectProviderDate.getAppointmentTypes())
											{
												Long appointmentTypeId=appointmentType.getId();
												String checkedString=WebUtils.getCheckedString(appointmentTypeId.equals(previouslySelectedAppointmentTypeId));
						%>
									<input type="radio" name="appointmentTypeId" value="<%=appointmentTypeId%>" <%=checkedString%>/> <%=appointmentType.getName()%>
									<br />
								<%
									}
								%>
						<br /><br />
						<%=LocaleUtils.getMessage(request, "NotesCommentsAboutAppointment")%>
						<br />
						<%
							String appointmentNotes=StringUtils.trimToEmpty((String)session.getAttribute("appointmentNotes"));
						%>
						<textarea name="appointmentNotes" style="width:24em;height:8em"><%=appointmentNotes%></textarea>
						<br /><br />
					</td>
	
					<td style="vertical-align:top">
						<br />
						<div id="date"></div>
						<%
							String previouslySelectedDate=StringUtils.trimToEmpty((String)session.getAttribute("dateSelectedString"));
						%>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#date').datepicker({ dateFormat: '<%=WebUtils.JS_ISO_DATE_FORMAT%>', defaultDate:'<%=previouslySelectedDate%>', onSelect: function(dateText, inst){ $('#dateSelected').val(dateText); } });
							});
						</script>
						<input type="hidden" id="dateSelected" name="dateSelected" value="<%=previouslySelectedDate%>" />
						<br />
						<%
							Boolean nextAvailableAppointment=(Boolean)session.getAttribute("nextAvailableAppointment");
											if (nextAvailableAppointment==null) nextAvailableAppointment=false;
											String checkedString=WebUtils.getCheckedString(nextAvailableAppointment);
						%>
						<table style="text-align:left">
							<tr>
								<td style="border:none;padding:0px"><input type="checkbox" id="nextAvailableAppointment" name="nextAvailableAppointment" <%=checkedString%> /></td>
								<td style="border:none;padding:0px"><%=LocaleUtils.getMessage(request, "ShowNextAvailableAppointmentAnyDay")%></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<input type="submit" value="<%=LocaleUtils.getMessage(request, "ShowAvailableAppointments")%>" />
			&nbsp;&nbsp;
			<input type="button" value="<%=LocaleUtils.getMessage(request, "Back")%>" onclick="history.go(-1)" />
		</div>
	</form>
</fieldset>

<br /><br /><br />

<fieldset class="yellowFieldSet">
	<legend><%=LocaleUtils.getMessage(request, "AppointmentHistory")%></legend>
	
	<table class="genericBorderedTable" style="background-color:white">
		<tr>
			<th><%=LocaleUtils.getMessage(request, "Date")%></th>
			<th><%=LocaleUtils.getMessage(request, "NotesCommentsAboutAppointment")%></th>
			<th></th>
		</tr>
		<%
			for (AppointmentTransfer appointmentTransfer : PreviousAppointments.getPreviousAppointments(MyOscarLoggedInInfo.getLoggedInInfo(session)))
			{
		%>
					<tr>
						<%
							String style="";
							if (PreviousAppointments.isCanceled(appointmentTransfer))
							{
								style="class=\"darkRedColor\" style=\"text-decoration:line-through\"";
							}
						%>
						<td <%=style%>><%=PreviousAppointments.getAppointmentTimeFormatted(appointmentTransfer)%></td>
						<td><%=PreviousAppointments.getNotesLimitedAndEscapeHtml(appointmentTransfer)%></td>
						<td>
							<%
								if (PreviousAppointments.canCancel(appointmentTransfer))
								{
									%>
										<input type="button" value="<%=LocaleUtils.getMessage(request, "CancelThisAppointment")%>" onclick="if (confirm('<%=LocaleUtils.getMessage(request, "AppointmentCancelConfirm") %>')) document.location.href='cancel_appointment.jsp?appointmentId=<%=appointmentTransfer.getId()%>'" />
									<%
								}
							
								if (PreviousAppointments.isCanceled(appointmentTransfer))
								{
									%>
										<span class="darkRedColor">
											<%=LocaleUtils.getMessage(request, "Cancelled")%>
										</span>
									<%
								}
							%>
						</td>
					</tr> 
				<%
			}
		%>
	</table>
</fieldset>

<%@include file="/resources/html_bottom.jspf"%>
