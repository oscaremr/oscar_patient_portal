<%@include file="/resources/html_top.jspf"%>

<h2>Welcome To Test Clinic</h2>
<br />

Our specialised medical clinic provides for all your blah blah needs.
<br /><br />

<h3>Hours of operations</h3>
Mon-Fri : 10am to 2:30pm
<br />

<h3>Address</h3>
1234 Fake St
Iqaluit
Nunavut
<br />

<h3>Phone Number</h3>
123-4456-7789
<br /><br />

<h3>
	To book or manage your appointments online click here : <button onclick="document.location.href='<%=request.getContextPath()%>/application/appointments/manage_appointments.jsp'">Appointments</button>
</h3>

<%@include file="/resources/html_bottom.jspf"%>
