<%@include file="/resources/html_top.jspf" %>

No one should actually be using this page, this is a development testing page. No need to make this pretty.
<br /><br />
<form method="post" action="index.jsp">
	MyOscar User Id (not userName) <input type="text" name="userId" />
	<br />
	MyOscar securityToken / password <input type="text" name="securityToken" />
	<br />
	<input type="submit" value="Test Component" />
</form>

<%@include file="/resources/html_bottom.jspf" %>
