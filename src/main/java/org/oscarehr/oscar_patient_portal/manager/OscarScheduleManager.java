package org.oscarehr.oscar_patient_portal.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.BookingSource;
import org.oscarehr.ws.DayWorkScheduleTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ScheduleTemplateCodeTransfer;
import org.oscarehr.ws.ScheduleWs;
import org.w3c.dom.Node;

public final class OscarScheduleManager
{
	private static final int MAX_OBJECTS_TO_CACHE = 4;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	private static final String CACHE_KEY_PLACE_HOLDER = "CACHE_KEY_PLACE_HOLDER";

	/**
	 * Single item cache, we only use the cache so it refreshes the data every once in a while. So the key is just CACHE_KEY_PLACE_HOLDER
	 */
	private static QueueCache<String, List<ScheduleTemplateCodeTransfer>> templateCodeCache = new QueueCache<String, List<ScheduleTemplateCodeTransfer>>(4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE, null);

	private static ArrayList<AppointmentType> appointmentTypes = initialiseAppointmentTypes();

	private static final ArrayList<Character> allowableTimeCodes = initTimeCodes();

	private static ArrayList<AppointmentType> initialiseAppointmentTypes()
	{
		ArrayList<AppointmentType> results = new ArrayList<AppointmentType>();

		ArrayList<Node> appointmentTypeNodes = ConfigXmlUtils.getPropertyNodeList("oscar_patient_portal", "appointment_type");
		for (Node tempNode : appointmentTypeNodes)
		{
			results.add(AppointmentType.fromXml(tempNode));
		}

		return(results);
	}

	private static ArrayList<Character> initTimeCodes()
	{
		String temp = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "allowable_time_codes");
		String[] tempSplit = temp.split(",");
		ArrayList<Character> allowableTimeCodes = new ArrayList<Character>();
		for (String s : tempSplit)
		{
			s = StringUtils.trimToNull(s);
			if (s != null && s.length() > 0)
			{
				allowableTimeCodes.add(s.charAt(0));
			}
		}

		return(allowableTimeCodes);
	}

	public static ArrayList<Character> getAllowableTimeCodes()
	{
		return(allowableTimeCodes);
	}

	public static List<ScheduleTemplateCodeTransfer> getScheduleTemplateCodes() throws NotAuthorisedException_Exception
	{
		List<ScheduleTemplateCodeTransfer> scheduleTemplateCodeTransfers = templateCodeCache.get(CACHE_KEY_PLACE_HOLDER);
		if (scheduleTemplateCodeTransfers == null)
		{
			ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
			scheduleTemplateCodeTransfers = scheduleWs.getScheduleTemplateCodes();
			templateCodeCache.put(CACHE_KEY_PLACE_HOLDER, scheduleTemplateCodeTransfers);
		}

		return(scheduleTemplateCodeTransfers);
	}

	public static ScheduleTemplateCodeTransfer getScheduleTemplateCode(char code) throws NotAuthorisedException_Exception
	{
		List<ScheduleTemplateCodeTransfer> allResults = getScheduleTemplateCodes();
		for (ScheduleTemplateCodeTransfer scheduleTemplateCodeTransfer : allResults)
		{
			char c = (char) scheduleTemplateCodeTransfer.getCode().intValue();
			if (c == code) return(scheduleTemplateCodeTransfer);
		}

		return(null);
	}

	public static List<AppointmentTransfer> getAppointments(String providerNo, Calendar date) throws NotAuthorisedException_Exception
	{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		List<AppointmentTransfer> appointmentTransfers = scheduleWs.getAppointmentsForProvider(providerNo, date);
		return(appointmentTransfers);
	}

	public static DayWorkScheduleTransfer getDayWorkSchedule(String providerNo, Calendar date) throws NotAuthorisedException_Exception
	{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		DayWorkScheduleTransfer dayWorkScheduleTransfer = scheduleWs.getDayWorkSchedule(providerNo, date);
		return(dayWorkScheduleTransfer);
	}

	public static List<AppointmentType> getAppointmentTypes()
	{
		return(appointmentTypes);
	}

	public static AppointmentType getAppointmentType(Long appointmentTypeId)
	{
		for (AppointmentType appointmentType : appointmentTypes)
		{
			if (appointmentTypeId.equals(appointmentType.getId())) return(appointmentType);
		}

		return(null);
	}

	public static Integer addAppointment(Calendar startTime, Calendar endTime, Integer demographicId, String name, String notes, String providerNo) throws NotAuthorisedException_Exception
	{
		AppointmentTransfer appointment = new AppointmentTransfer();
		appointment.setAppointmentEndDateTime(endTime);
		appointment.setAppointmentStartDateTime(startTime);
		appointment.setDemographicNo(demographicId);
		appointment.setName(name);
		appointment.setNotes(notes);
		appointment.setProviderNo(providerNo);
		appointment.setStatus("t"); // dunno what that means but seems to be what everyone else uses
		appointment.setBookingSource(BookingSource.MYOSCAR_SELF_BOOKING);

		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		Integer result = scheduleWs.addAppointment(appointment);
		return(result);
	}

	public static List<AppointmentTransfer> getAppointmentsForPatient(Integer demographicId, int startIndex, int itemsToReturn) throws NotAuthorisedException_Exception
	{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		List<AppointmentTransfer> appointmentTransfers = scheduleWs.getAppointmentsForPatient(demographicId, startIndex, itemsToReturn);
		return(appointmentTransfers);
	}

	public static void updateAppointment(AppointmentTransfer theAppointment) throws NotAuthorisedException_Exception
	{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		scheduleWs.updateAppointment(theAppointment);
	}
}
