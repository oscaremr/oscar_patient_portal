package org.oscarehr.oscar_patient_portal.manager;

import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.NotAuthorisedException_Exception;

public final class OscarDemographicManager
{
	public static DemographicTransfer getDemographicByMyOscarUserName(String myOscarUserName) throws NotAuthorisedException_Exception
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs();
		DemographicTransfer result = demographicWs.getDemographicByMyOscarUserName(myOscarUserName);
		return(result);
	}
}
