package org.oscarehr.oscar_patient_portal.manager;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.FacilityTransfer;
import org.oscarehr.ws.FacilityWs;
import org.oscarehr.ws.NotAuthorisedException_Exception;

public final class OscarFacilityManager
{
	private static final int MAX_OBJECTS_TO_CACHE = 4;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	private static final String DEFAULT_FACILITY_CACHE_KEY = "DEFAULT_FACILITY_CACHE_KEY";

	/**
	 * key is not really relevant, we're just using this as a time out cache.
	 */
	private static QueueCache<String, FacilityTransfer> facilityCache = new QueueCache<String, FacilityTransfer>(4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE, null);

	public static FacilityTransfer getDefaultFacility() throws NotAuthorisedException_Exception
	{
		FacilityTransfer result = facilityCache.get(DEFAULT_FACILITY_CACHE_KEY);

		if (result == null)
		{
			FacilityWs facilityWs = OscarServerWebServicesManager.getFacilityWs();
			result = facilityWs.getDefaultFacilities();
			facilityCache.put(DEFAULT_FACILITY_CACHE_KEY, result);
		}

		return(result);
	}
}
