package org.oscarehr.oscar_patient_portal.manager;

import java.util.ArrayList;
import java.util.HashMap;

import org.oscarehr.util.XmlUtils;
import org.w3c.dom.Node;

public final class AppointmentType
{
	private Long id;
	private String name;
	private int defaultDurationMinutes;
	private HashMap<String, Integer> roleDurations = new HashMap<String, Integer>();

	public Long getId()
	{
		return(id);
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return(name);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getDefaultDurationMinutes()
	{
		return(defaultDurationMinutes);
	}

	public void setDefaultDurationMinutes(int defaultDurationMinutes)
	{
		this.defaultDurationMinutes = defaultDurationMinutes;
	}

	public HashMap<String, Integer> getRoleDurations()
	{
		return(roleDurations);
	}

	public void setRoleDurations(HashMap<String, Integer> roleDurations)
	{
		this.roleDurations = roleDurations;
	}

	public static AppointmentType fromXml(Node node)
	{
		AppointmentType result = new AppointmentType();

		result.id = XmlUtils.getChildNodeLongContents(node, "id");
		result.name = XmlUtils.getChildNodeTextContents(node, "name");
		result.defaultDurationMinutes = XmlUtils.getChildNodeIntegerContents(node, "default_duration_minutes");

		ArrayList<Node> roles = XmlUtils.getChildNodes(node, "role");
		for (Node roleNode : roles)
		{
			String roleName = XmlUtils.getChildNodeTextContents(roleNode, "role_name");
			Integer durationMinutes = XmlUtils.getChildNodeIntegerContents(roleNode, "duration_minutes");

			result.roleDurations.put(roleName, durationMinutes);
		}

		return(result);
	}
}
