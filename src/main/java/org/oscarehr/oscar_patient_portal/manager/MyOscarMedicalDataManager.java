package org.oscarehr.oscar_patient_portal.manager;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.myoscar.client.ws_manager.MyOscarServerWebServicesManager;
import org.oscarehr.myoscar_server.ws.InvalidRequestException_Exception;
import org.oscarehr.myoscar_server.ws.ItemAlreadyExistsException_Exception;
import org.oscarehr.myoscar_server.ws.ItemCompletedException_Exception;
import org.oscarehr.myoscar_server.ws.MedicalDataTransfer4;
import org.oscarehr.myoscar_server.ws.MedicalDataWs;
import org.oscarehr.myoscar_server.ws.NoSuchItemException_Exception;
import org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception;
import org.oscarehr.myoscar_server.ws.UnsupportedEncodingException_Exception;
import org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo;
import org.oscarehr.util.QueueCache;

public final class MyOscarMedicalDataManager
{
	private static final int MAX_OBJECTS_TO_CACHE = 256;
	private static final int MAX_OBJECT_SIZE_TO_CACHE = 10240;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	/**
	 * Cache key must be qualified so 2 people don't share a cached item, even if it's the same thing,
	 * the request must be made to the server so the access is logged and authentication is checked.
	 * 
	 * MAX_CACHE_ITEM_SIZE*items to queue = max memory used for cache, so 10,000 items at 10,240 bytes = 102,400,000 = 102 megs of rams
	 */
	private static QueueCache<String, MedicalDataTransfer4> medicalDataCache = new QueueCache<String, MedicalDataTransfer4>(4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE, null);

	private static String getCacheKey(MyOscarLoggedInInfo loggedInInfo, Long medicalDataId)
	{
		return(loggedInInfo.getLoggedInSessionId() + ':' + loggedInInfo.getLoggedInPersonId() + ':' + medicalDataId);
	}

	public static void updateMedicalData(MyOscarLoggedInInfo loggedInInfo, MedicalDataTransfer4 medicalDataTransfer) throws NotAuthorisedException_Exception, NoSuchItemException_Exception, ItemCompletedException_Exception, UnsupportedEncodingException_Exception, InvalidRequestException_Exception
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		medicalDataWs.updateMedicalData5(medicalDataTransfer);
		String cacheKey = getCacheKey(loggedInInfo, medicalDataTransfer.getId());
		medicalDataCache.remove(cacheKey);
	}

	public static MedicalDataTransfer4 getMedicalData(MyOscarLoggedInInfo loggedInInfo, Long ownerPersonId, Long medicalDataId) throws NotAuthorisedException_Exception, NoSuchItemException_Exception
	{
		// check cache for item
		String cacheKey = getCacheKey(loggedInInfo, medicalDataId);
		MedicalDataTransfer4 medicalDataTransfer = medicalDataCache.get(cacheKey);

		if (medicalDataTransfer == null)
		{
			MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
			medicalDataTransfer = medicalDataWs.getMedicalData5(ownerPersonId, medicalDataId);

			// if the item is small cache it.
			if (medicalDataTransfer.getData().length() < MAX_OBJECT_SIZE_TO_CACHE)
			{
				medicalDataCache.put(cacheKey, medicalDataTransfer);
			}
		}

		return(medicalDataTransfer);
	}

	public static List<MedicalDataTransfer4> getMedicalData(MyOscarLoggedInInfo loggedInInfo, Long ownerPersonId, String medicalDataType, Boolean active, int startIndex, int itemsToReturn)
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		List<MedicalDataTransfer4> results = medicalDataWs.getMedicalDataByType2(ownerPersonId, medicalDataType, active, startIndex, itemsToReturn);
		return(results);
	}

	public static List<MedicalDataTransfer4> getMedicalData(MyOscarLoggedInInfo loggedInInfo, Long ownerPersonId, String medicalDataType, Boolean active, Calendar startDate, Calendar endDate)
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		List<MedicalDataTransfer4> results = medicalDataWs.getMedicalDataByTypeAndDateRange3(ownerPersonId, medicalDataType, active, startDate, endDate, true);
		return(results);
	}

	public static Long addMedicalData(MyOscarLoggedInInfo loggedInInfo, MedicalDataTransfer4 medicalDataTransfer) throws NotAuthorisedException_Exception, ItemAlreadyExistsException_Exception, UnsupportedEncodingException_Exception
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		Long id = medicalDataWs.addMedicalData4(medicalDataTransfer);
		return(id);
	}

	public static int getMedicalDataCount(MyOscarLoggedInInfo loggedInInfo, Long ownerPersonId, String medicalDataType, Boolean active)
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		int count = medicalDataWs.getMedicalDataCount(ownerPersonId, medicalDataType, active);
		return(count);
	}

	public static void setMedicalDataActive(MyOscarLoggedInInfo loggedInInfo, Long ownerId, Long medicalDataId, Boolean active) throws NotAuthorisedException_Exception, NoSuchItemException_Exception
	{
		MedicalDataWs medicalDataWs = MyOscarServerWebServicesManager.getMedicalDataWs(loggedInInfo);
		medicalDataWs.setMedicalDataActive2(ownerId, medicalDataId, active);

		medicalDataCache.remove(getCacheKey(loggedInInfo, medicalDataId));
	}
}
