package org.oscarehr.oscar_patient_portal.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.util.QueueCache;
import org.oscarehr.util.XmlUtils;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;
import org.oscarehr.ws.ProviderWs;
import org.w3c.dom.Node;

public final class OscarProviderManager
{
	private static final int MAX_OBJECTS_TO_CACHE = 4;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	/**
	 * key is true=active | false=inactive providers list
	 */
	private static QueueCache<Boolean, List<ProviderTransfer>> providerCache = new QueueCache<Boolean, List<ProviderTransfer>>(4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE, null);

	/**
	 * key=providerNo (the main providers No)
	 * value=list of providerNo's who are on that providers team excluding the main provider.
	 */
	private static final HashMap<String, List<String>> allowedProvidersAndTeam = initAllowedProviderAndTeam();

	private static HashMap<String, List<String>> initAllowedProviderAndTeam()
	{
		HashMap<String, List<String>> result = new HashMap<String, List<String>>();

		ArrayList<Node> nodes = ConfigXmlUtils.getPropertyNodeList("oscar_patient_portal", "allowed_provider");
		if (nodes != null)
		{
			for (Node node : nodes)
			{
				String mainProviderNo = XmlUtils.getChildNodeTextContents(node, "provider_no");
				ArrayList<String> teamMembers = XmlUtils.getChildNodesTextContents(node, "team_member");
				result.put(mainProviderNo, teamMembers);
			}
		}

		return(result);
	}

	public static List<ProviderTransfer> getProviders(boolean active) throws NotAuthorisedException_Exception
	{
		List<ProviderTransfer> results = providerCache.get(active);

		if (results == null)
		{
			ProviderWs providerWs = OscarServerWebServicesManager.getProviderWs();
			results = providerWs.getProviders(active);
			providerCache.put(active, results);
		}

		return(results);
	}

	public static ProviderTransfer getProvider(String providerId) throws NotAuthorisedException_Exception
	{
		// check active providers
		List<ProviderTransfer> results = getProviders(true);
		for (ProviderTransfer providerTransfer : results)
		{
			if (providerTransfer.getProviderNo().equals(providerId)) return(providerTransfer);
		}

		// check inactive providers
		results = getProviders(false);
		for (ProviderTransfer providerTransfer : results)
		{
			if (providerTransfer.getProviderNo().equals(providerId)) return(providerTransfer);
		}

		return(null);
	}

	public static HashMap<String, List<String>> getAllowedProvidersAndTeam()
	{
		return(allowedProvidersAndTeam);
	}

	public static boolean isProviderInAllowedList(String providerNo)
	{
		for (Entry<String, List<String>> entry : allowedProvidersAndTeam.entrySet())
		{
			if (providerNo.equals(entry.getKey())) return(true);

			if (entry.getValue().contains(providerNo)) return(true);
		}

		return(false);
	}
}
