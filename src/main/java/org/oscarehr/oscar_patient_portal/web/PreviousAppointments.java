package org.oscarehr.oscar_patient_portal.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringEscapeUtils;
import org.oscarehr.myoscar.client.ws_manager.MessageManager;
import org.oscarehr.myoscar.util.LocaleUtils;
import org.oscarehr.myoscar_server.ws.ItemAlreadyExistsException_Exception;
import org.oscarehr.myoscar_server.ws.MedicalDataTransfer4;
import org.oscarehr.myoscar_server.ws.PersonTransfer2;
import org.oscarehr.myoscar_server.ws.UnsupportedEncodingException_Exception;
import org.oscarehr.oscar_patient_portal.manager.MyOscarMedicalDataManager;
import org.oscarehr.oscar_patient_portal.manager.OscarDemographicManager;
import org.oscarehr.oscar_patient_portal.manager.OscarFacilityManager;
import org.oscarehr.oscar_patient_portal.manager.OscarProviderManager;
import org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager;
import org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo;
import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.util.XmlUtils;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.FacilityTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;
import org.w3c.dom.Document;

public final class PreviousAppointments
{
	private static SimpleDateFormat formatAppointment = new SimpleDateFormat("yyyy-MM-dd, hh:mm a");

	/**
	 * This method only returns previous appointments which are available to the online booking system,
	 * i.e. it will not return an appointment if the provider for that appointment is not an "online available provider"
	 */
	public static List<AppointmentTransfer> getPreviousAppointments(MyOscarLoggedInInfo loggedInInfo) throws NotAuthorisedException_Exception, org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception
	{
		PersonTransfer2 myOscarPersonTransfer = loggedInInfo.getLoggedInPerson();
		DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByMyOscarUserName(myOscarPersonTransfer.getUserName());

		// for now we'll hard code to last 30 appointments
		List<AppointmentTransfer> tempResult = OscarScheduleManager.getAppointmentsForPatient(oscarDemographic.getDemographicNo(), 0, 30);
		List<AppointmentTransfer> results = new ArrayList<AppointmentTransfer>();

		// we need to filter for providers using online system
		for (AppointmentTransfer appointmentTransfer : tempResult)
		{
			if (OscarProviderManager.isProviderInAllowedList(appointmentTransfer.getProviderNo()))
			{
				results.add(appointmentTransfer);
			}
		}

		return(results);
	}

	public static String getAppointmentTimeFormatted(AppointmentTransfer appointmentTransfer)
	{
		return(formatAppointment.format(appointmentTransfer.getAppointmentStartDateTime().getTime()));
	}

	public static String getNotesLimitedAndEscapeHtml(AppointmentTransfer appointmentTransfer)
	{
		String s = appointmentTransfer.getNotes();

		if (s == null) return("");

		if (s.length() > 100) s = s.substring(0, 100) + "...";

		return(StringEscapeUtils.escapeHtml(s));
	}

	public static boolean canCancel(AppointmentTransfer appointmentTransfer)
	{
		// can only cancel future appointments
		if (appointmentTransfer.getAppointmentStartDateTime().before(new GregorianCalendar())) return(false);

		// can not cancel cancelled appointments
		if (isCanceled(appointmentTransfer)) return(false);

		return(true);
	}

	public static boolean isCanceled(AppointmentTransfer appointmentTransfer)
	{
		return(appointmentTransfer.getStatus().equals("C"));
	}

	public static void cancelAppointment(MyOscarLoggedInInfo loggedInInfo, Integer appointmentId) throws NotAuthorisedException_Exception, org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception, ParserConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException, ItemAlreadyExistsException_Exception, UnsupportedEncodingException_Exception
	{
		// must double check it is an appointment they are allowed to cancel as a post can be altered.
		List<AppointmentTransfer> existingAppointments = getPreviousAppointments(loggedInInfo);
		AppointmentTransfer theAppointment = null;
		for (AppointmentTransfer appointmentTemp : existingAppointments)
		{
			if (appointmentId.equals(appointmentTemp.getId()))
			{
				theAppointment = appointmentTemp;
				break;
			}
		}

		if (theAppointment == null) throw(new IllegalStateException("Some one posting a dodgy appointment that isn't theirs? appointmentId=" + appointmentId + ", loggedInInfo.myOscarUserId=" + loggedInInfo.getLoggedInPersonId()));

		// apparently status "C" means cancelled
		theAppointment.setStatus("C");

		OscarScheduleManager.updateAppointment(theAppointment);

		// send a copy to myoscar
		ProviderTransfer provider = OscarProviderManager.getProvider(theAppointment.getProviderNo());
		FacilityTransfer facilityTransfer = OscarFacilityManager.getDefaultFacility();
		sendRecordToMyOscar(loggedInInfo, theAppointment.getAppointmentStartDateTime(), provider, appointmentId, facilityTransfer);

		// send a myoscar message
		sendMyOscarMessageAboutAppointment(loggedInInfo, facilityTransfer, theAppointment.getAppointmentStartDateTime(), provider);
	}

	private static void sendRecordToMyOscar(MyOscarLoggedInInfo loggedInInfo, Calendar startTime, ProviderTransfer oscarProvider, Integer appointmentId, FacilityTransfer facilityTransfer) throws ParserConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException, ItemAlreadyExistsException_Exception, org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception, UnsupportedEncodingException_Exception
	{
		MedicalDataTransfer4 medicalDataTransfer = new MedicalDataTransfer4();
		medicalDataTransfer.setActive(true);
		medicalDataTransfer.setCompleted(true);

		Document doc = XmlUtils.newDocument("OscarAppointmentCancel");
		XmlUtils.appendChildToRoot(doc, "server", OscarServerWebServicesManager.getMyoscarServerBaseUrl());
		XmlUtils.appendChildToRoot(doc, "facilityName", facilityTransfer.getName());
		XmlUtils.appendChildToRoot(doc, "appointmentId", appointmentId.toString());
		XmlUtils.appendChildToRoot(doc, "providerName", oscarProvider.getLastName() + ", " + oscarProvider.getFirstName());
		medicalDataTransfer.setData(XmlUtils.toString(doc, false));

		medicalDataTransfer.setDateOfData(startTime);
		medicalDataTransfer.setMedicalDataType(BookAppointment.MYOSCAR_APPOINTMENT_MEDICAL_DATA_TYPE);
		medicalDataTransfer.setObserverOfDataPersonId(loggedInInfo.getLoggedInPersonId());
		medicalDataTransfer.setOriginalSourceId(facilityTransfer.getName() + ":appointment_cancel:" + appointmentId);
		medicalDataTransfer.setOwningPersonId(loggedInInfo.getLoggedInPersonId());

		MyOscarMedicalDataManager.addMedicalData(loggedInInfo, medicalDataTransfer);
	}

	private static void sendMyOscarMessageAboutAppointment(MyOscarLoggedInInfo loggedInInfo, FacilityTransfer facilityTransfer, Calendar startTime, ProviderTransfer providerTransfer) throws org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception, UnsupportedEncodingException_Exception
	{
		String subject = LocaleUtils.getMessage(loggedInInfo.getLocale(), "EmailSubject_AppointmentCancel");

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, yyyy-MM-dd, hh:mm a", loggedInInfo.getLocale());
		sb.append(subject);
		sb.append("\n");
		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Date"));
		sb.append(" : ");
		sb.append(sdf.format(startTime.getTime()));
		sb.append("\n");

		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Location"));
		sb.append(" : ");
		sb.append(facilityTransfer.getName());
		sb.append("\n");

		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Provider"));
		sb.append(" : ");
		sb.append(providerTransfer.getLastName() + ", " + providerTransfer.getFirstName());
		sb.append("\n");

		MessageManager.sendMessage(loggedInInfo, loggedInInfo.getLoggedInPersonId(), subject, sb.toString());
	}
}
