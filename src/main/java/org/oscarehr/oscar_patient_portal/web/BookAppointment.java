package org.oscarehr.oscar_patient_portal.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.xml.parsers.ParserConfigurationException;

import org.oscarehr.myoscar.client.ws_manager.MessageManager;
import org.oscarehr.myoscar.util.LocaleUtils;
import org.oscarehr.myoscar_server.ws.ItemAlreadyExistsException_Exception;
import org.oscarehr.myoscar_server.ws.MedicalDataTransfer4;
import org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception;
import org.oscarehr.myoscar_server.ws.PersonTransfer2;
import org.oscarehr.myoscar_server.ws.UnsupportedEncodingException_Exception;
import org.oscarehr.oscar_patient_portal.manager.AppointmentType;
import org.oscarehr.oscar_patient_portal.manager.MyOscarMedicalDataManager;
import org.oscarehr.oscar_patient_portal.manager.OscarDemographicManager;
import org.oscarehr.oscar_patient_portal.manager.OscarFacilityManager;
import org.oscarehr.oscar_patient_portal.manager.OscarProviderManager;
import org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager;
import org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo;
import org.oscarehr.oscar_patient_portal.utils.OscarServerWebServicesManager;
import org.oscarehr.util.XmlUtils;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.FacilityTransfer;
import org.oscarehr.ws.ProviderTransfer;
import org.w3c.dom.Document;

public final class BookAppointment
{
	public static final String MYOSCAR_APPOINTMENT_MEDICAL_DATA_TYPE = "OSCAR.appointment";

	public static Integer bookAppointment(MyOscarLoggedInInfo loggedInInfo, Calendar startTime, String providerId, Long appointmentTypeId, String notes) throws NotAuthorisedException_Exception, org.oscarehr.ws.NotAuthorisedException_Exception, ParserConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException, ItemAlreadyExistsException_Exception, UnsupportedEncodingException_Exception
	{
		AppointmentType appointmentType = OscarScheduleManager.getAppointmentType(appointmentTypeId);
		ProviderTransfer provider = OscarProviderManager.getProvider(providerId);
		FacilityTransfer facilityTransfer = OscarFacilityManager.getDefaultFacility();

		int durationMinutes = appointmentType.getDefaultDurationMinutes();
		Integer temp = appointmentType.getRoleDurations().get(provider.getSpecialty());
		if (temp != null) durationMinutes = temp;

		Calendar endTime = (Calendar) startTime.clone();
		endTime.add(Calendar.MINUTE, durationMinutes);
		endTime.getTimeInMillis();

		// book appointment in oscar
		Integer resultId = bookAppointmentInOscar(loggedInInfo, startTime, providerId, notes, endTime);

		// send a copy to myoscar
		sendRecordToMyOscar(loggedInInfo, startTime, provider, appointmentType, resultId, facilityTransfer);

		// send a myoscar message
		sendMyOscarMessageAboutAppointment(loggedInInfo, facilityTransfer, startTime, provider, appointmentType, notes);

		return(resultId);
	}

	private static void sendMyOscarMessageAboutAppointment(MyOscarLoggedInInfo loggedInInfo, FacilityTransfer facilityTransfer, Calendar startTime, ProviderTransfer providerTransfer, AppointmentType appointmentType, String notes) throws NotAuthorisedException_Exception, UnsupportedEncodingException_Exception
	{
		String subject = LocaleUtils.getMessage(loggedInInfo.getLocale(), "EmailSubject_AppointmentDetails");

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, yyyy-MM-dd, hh:mm a", loggedInInfo.getLocale());
		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Date"));
		sb.append(" : ");
		sb.append(sdf.format(startTime.getTime()));
		sb.append("\n\n");

		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Location"));
		sb.append(" : ");
		sb.append(facilityTransfer.getName());
		sb.append("\n");
		if (facilityTransfer.getContactPhone() != null)
		{
			sb.append(facilityTransfer.getContactPhone());
			sb.append("\n");
		}
		sb.append("\n");

		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "Provider"));
		sb.append(" : ");
		sb.append(providerTransfer.getLastName() + ", " + providerTransfer.getFirstName());
		sb.append("\n\n");

		sb.append(LocaleUtils.getMessage(loggedInInfo.getLocale(), "ReasonForAppointment"));
		sb.append(" : ");
		sb.append(appointmentType.getName());
		sb.append("\n");
		if (notes != null) sb.append(notes);

		MessageManager.sendMessage(loggedInInfo, loggedInInfo.getLoggedInPersonId(), subject, sb.toString());
	}

	private static void sendRecordToMyOscar(MyOscarLoggedInInfo loggedInInfo, Calendar startTime, ProviderTransfer oscarProvider, AppointmentType appointmentType, Integer resultId, FacilityTransfer facilityTransfer) throws ParserConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException, NotAuthorisedException_Exception, ItemAlreadyExistsException_Exception, UnsupportedEncodingException_Exception
	{
		MedicalDataTransfer4 medicalDataTransfer = new MedicalDataTransfer4();
		medicalDataTransfer.setActive(true);
		medicalDataTransfer.setCompleted(true);

		Document doc = XmlUtils.newDocument("OscarAppointment");
		XmlUtils.appendChildToRoot(doc, "server", OscarServerWebServicesManager.getMyoscarServerBaseUrl());
		XmlUtils.appendChildToRoot(doc, "facilityName", facilityTransfer.getName());
		XmlUtils.appendChildToRoot(doc, "appointmentId", resultId.toString());
		XmlUtils.appendChildToRoot(doc, "appointmentType", appointmentType.getName());
		XmlUtils.appendChildToRoot(doc, "providerName", oscarProvider.getLastName() + ", " + oscarProvider.getFirstName());
		medicalDataTransfer.setData(XmlUtils.toString(doc, false));

		medicalDataTransfer.setDateOfData(startTime);
		medicalDataTransfer.setMedicalDataType(MYOSCAR_APPOINTMENT_MEDICAL_DATA_TYPE);
		medicalDataTransfer.setObserverOfDataPersonId(loggedInInfo.getLoggedInPersonId());
		medicalDataTransfer.setOriginalSourceId(facilityTransfer.getName() + ":appointment:" + resultId);
		medicalDataTransfer.setOwningPersonId(loggedInInfo.getLoggedInPersonId());

		MyOscarMedicalDataManager.addMedicalData(loggedInInfo, medicalDataTransfer);
	}

	private static Integer bookAppointmentInOscar(MyOscarLoggedInInfo loggedInInfo, Calendar startTime, String providerId, String notes, Calendar endTime) throws NotAuthorisedException_Exception, org.oscarehr.ws.NotAuthorisedException_Exception
	{
		PersonTransfer2 myOscarPersonTransfer = loggedInInfo.getLoggedInPerson();
		DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByMyOscarUserName(myOscarPersonTransfer.getUserName());
		Integer resultId = OscarScheduleManager.addAppointment(startTime, endTime, oscarDemographic.getDemographicNo(), oscarDemographic.getLastName() + ", " + oscarDemographic.getFirstName(), notes, providerId);
		return resultId;
	}
}