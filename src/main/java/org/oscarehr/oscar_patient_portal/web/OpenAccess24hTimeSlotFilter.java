package org.oscarehr.oscar_patient_portal.web;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class OpenAccess24hTimeSlotFilter implements AvailableTimeSlotFilter
{
	@Override
	public ArrayList<Calendar> filterAvailableTimeSlots(String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, ArrayList<Calendar> currentlyAllowedTimeSlots)
	{
		// allowed time codes
		ArrayList<Calendar> filteredResults = new ArrayList<Calendar>();
		for (Calendar entry : currentlyAllowedTimeSlots)
		{
			if (isAllowedTime(dayWorkScheduleTransfer, entry))
			{
				filteredResults.add(entry);
			}
		}
		return(filteredResults);
	}

	private boolean isAllowedTime(DayWorkScheduleTransfer dayWorkScheduleTransfer, Calendar timeSlot)
	{
		// if the time is open access time slot
		//    the time slot must be with in 24 hours of right now
		// else it's allowed

		for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots())
		{
			// find the time slot type for this entry
			if (entry.getDate().equals(timeSlot))
			{
				Integer i = entry.getScheduleCode();
				char c = (char) (i.intValue());

				// if it's an open access timeslot
				if ('o' == c)
				{
					// it's allowed if it's with in 24 hours
					// it's not allowed if it's anything else
					return(entry.getDate().getTimeInMillis() < (System.currentTimeMillis() + DateUtils.MILLIS_PER_DAY));
				}
			}
		}

		return(true);
	}
}