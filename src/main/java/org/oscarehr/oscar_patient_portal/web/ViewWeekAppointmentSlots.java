package org.oscarehr.oscar_patient_portal.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.time.DateFormatUtils;
import org.oscarehr.oscar_patient_portal.manager.OscarProviderManager;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;

public final class ViewWeekAppointmentSlots
{
	public static final int DAYS_TO_OFFSET_DISPLAY = 2;

	public static ArrayList<String> get1WeekDatesIsoFormat(String dateSelectedIsoFormat) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
		Date date = sdf.parse(dateSelectedIsoFormat);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(GregorianCalendar.DAY_OF_YEAR, -DAYS_TO_OFFSET_DISPLAY);

		ArrayList<String> results = new ArrayList<String>();
		for (int i = 0; i < 7; i++)
		{
			cal.getTime();
			results.add(DateFormatUtils.ISO_DATE_FORMAT.format(cal));
			cal.add(GregorianCalendar.DAY_OF_YEAR, 1);
		}

		return(results);
	}

	public static String getProviderFullNameHtmlEscaped(String providerId) throws NotAuthorisedException_Exception
	{
		ProviderTransfer tempResult = OscarProviderManager.getProvider(providerId);
		StringBuilder sb = new StringBuilder();

		if (tempResult.getFirstName() != null) sb.append(tempResult.getFirstName());

		if (tempResult.getLastName() != null)
		{
			if (sb.length() > 0) sb.append(' ');

			sb.append(tempResult.getLastName());
		}

		return(sb.toString());
	}
}
