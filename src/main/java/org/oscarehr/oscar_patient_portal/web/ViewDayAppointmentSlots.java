package org.oscarehr.oscar_patient_portal.web;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.oscarehr.myoscar.util.MiscUtils;
import org.oscarehr.oscar_patient_portal.manager.AppointmentType;
import org.oscarehr.oscar_patient_portal.manager.OscarProviderManager;
import org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;

public final class ViewDayAppointmentSlots
{
	private static Logger logger = MiscUtils.getLogger();

	public static Calendar getCalendar(String isoDate) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
		Date date = sdf.parse(isoDate);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return(cal);
	}

	private static String getDebugString(AppointmentTransfer appointmentTransfer)
	{
		StringBuilder sb = new StringBuilder();

		sb.append('[');
		sb.append("id=");
		sb.append(appointmentTransfer.getId());
		sb.append(',');
		sb.append("startTime=");
		sb.append(appointmentTransfer.getAppointmentStartDateTime() == null ? "null" : appointmentTransfer.getAppointmentStartDateTime().getTime());
		sb.append(',');
		sb.append("endTime=");
		sb.append(appointmentTransfer.getAppointmentEndDateTime() == null ? "null" : appointmentTransfer.getAppointmentEndDateTime().getTime());
		sb.append(']');

		return(sb.toString());
	}

	public static ArrayList<Calendar> getTimeSlots(String providerId, Long appointmentTypeId, Calendar date) throws NotAuthorisedException_Exception, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		// algorithm
		//----------
		// get the work schedule
		// check if it's a holiday, if so no appointments allowed
		// check which timecodes during the work day are allowed
		// make sure the appointment time is in the future
		// check conflicts with existing appointments
		// check for contiguous time sufficient to hold the required duration

		ProviderTransfer provider = OscarProviderManager.getProvider(providerId);
		AppointmentType appointmentType = OscarScheduleManager.getAppointmentType(appointmentTypeId);

		int durationMinutes = appointmentType.getDefaultDurationMinutes();
		Integer temp = appointmentType.getRoleDurations().get(provider.getSpecialty());
		if (temp != null) durationMinutes = temp;

		long requiredDurationMs = durationMinutes * 60 * 1000;

		// work schedule
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerId, date);
		debugDayWorkScheduleTransfer(dayWorkScheduleTransfer);
		if (dayWorkScheduleTransfer == null || dayWorkScheduleTransfer.isHoliday()) return(new ArrayList<Calendar>());

		ArrayList<Calendar> allowedResultsList = getAllowedTimesByType(dayWorkScheduleTransfer);
		debugTimeSlots("allowedTimesFilteredByType", allowedResultsList);

		allowedResultsList = getFutureTimesOnly(allowedResultsList);
		debugTimeSlots("allowedTimesFilteredByFutureTimes", allowedResultsList);

		allowedResultsList = getTimesNotAlreadyTaken(providerId, date, requiredDurationMs, allowedResultsList);
		debugTimeSlots("allowedTimesFilteredByExistingAppointments", allowedResultsList);

		allowedResultsList = getTimesWithSufficientContiguousTime(requiredDurationMs, dayWorkScheduleTransfer, allowedResultsList);
		debugTimeSlots("allowedTimesFilteredByContiguousDuration", allowedResultsList);

		// apply dynamic filters
		ArrayList<String> filterClassNames = ConfigXmlUtils.getPropertyStringList("oscar_patient_portal", "available_time_slot_filter_class");
		if (filterClassNames != null)
		{
			for (String className : filterClassNames)
			{
				@SuppressWarnings("unchecked")
				Class<AvailableTimeSlotFilter> filterClass = (Class<AvailableTimeSlotFilter>) Class.forName(className);
				AvailableTimeSlotFilter filterClassInstance = filterClass.newInstance();
				allowedResultsList = filterClassInstance.filterAvailableTimeSlots(providerId, appointmentTypeId, dayWorkScheduleTransfer, allowedResultsList);
				debugTimeSlots(filterClass.getSimpleName(), allowedResultsList);
			}
		}

		return(allowedResultsList);
	}

	private static void debugDayWorkScheduleTransfer(DayWorkScheduleTransfer dayWorkScheduleTransfer)
	{
		if (logger.isDebugEnabled())
		{
			if (dayWorkScheduleTransfer == null)
			{
				logger.debug("dayWorkScheduleTransfer=null");
				return;
			}

			logger.debug("dayWorkScheduleTransfer : getTimeSlotDurationMin=" + dayWorkScheduleTransfer.getTimeSlotDurationMin() + ", isHoliday=" + dayWorkScheduleTransfer.isHoliday());
			for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots())
			{
				Integer i = entry.getScheduleCode();
				char c = (char) (i.intValue());
				logger.debug("dayWorkScheduleTransfer : " + entry.getDate().getTime() + "=" + c);
			}
		}
	}

	private static void debugTimeSlots(String state, ArrayList<Calendar> timeSlots)
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("----- time slot state = " + state);
			for (Calendar cal : timeSlots)
			{
				logger.debug("--- " + DateFormatUtils.ISO_DATETIME_FORMAT.format(cal));
			}
		}
	}

	private static ArrayList<Calendar> getTimesWithSufficientContiguousTime(long requiredDurationMs, DayWorkScheduleTransfer dayWorkScheduleTransfer, ArrayList<Calendar> allowedTimesFilteredByExistingAppointments)
	{
		// check for enough contiguous time
		ArrayList<Calendar> allowedTimesFilteredByContiguousDuration = new ArrayList<Calendar>();
		if (dayWorkScheduleTransfer != null)
		{
			long workScheduleTimeSlotDurationMs = dayWorkScheduleTransfer.getTimeSlotDurationMin() * 60 * 1000;
			for (int i = 0; i < allowedTimesFilteredByExistingAppointments.size(); i++)
			{
				if (hasEnoughContiguousTime(i, requiredDurationMs, allowedTimesFilteredByExistingAppointments, workScheduleTimeSlotDurationMs))
				{
					allowedTimesFilteredByContiguousDuration.add(allowedTimesFilteredByExistingAppointments.get(i));
				}
			}
		}

		return allowedTimesFilteredByContiguousDuration;
	}

	private static ArrayList<Calendar> getTimesNotAlreadyTaken(String providerId, Calendar date, long requiredDurationMs, ArrayList<Calendar> allowedTimesFilteredByFutureTimes) throws NotAuthorisedException_Exception
	{
		// check conflict with existing appointments
		List<AppointmentTransfer> existingAppointments = OscarScheduleManager.getAppointments(providerId, date);
		ArrayList<Calendar> allowedTimesFilteredByExistingAppointments = new ArrayList<Calendar>();
		for (Calendar startTime : allowedTimesFilteredByFutureTimes)
		{
			if (!isThisTakenByExistingAppointment(startTime, requiredDurationMs, existingAppointments))
			{
				allowedTimesFilteredByExistingAppointments.add(startTime);
			}
		}
		return allowedTimesFilteredByExistingAppointments;
	}

	private static ArrayList<Calendar> getFutureTimesOnly(ArrayList<Calendar> allowedTimesFilteredByType)
	{
		// only allow times in the future
		ArrayList<Calendar> allowedTimesFilteredByFutureTimes = new ArrayList<Calendar>();
		Calendar now = new GregorianCalendar();
		for (Calendar startTime : allowedTimesFilteredByType)
		{
			if (startTime.after(now))
			{
				allowedTimesFilteredByFutureTimes.add(startTime);
			}
		}
		return allowedTimesFilteredByFutureTimes;
	}

	private static ArrayList<Calendar> getAllowedTimesByType(DayWorkScheduleTransfer dayWorkScheduleTransfer)
	{
		// allowed time codes
		ArrayList<Character> allowableTimeCodes = OscarScheduleManager.getAllowableTimeCodes();
		ArrayList<Calendar> allowedTimesFilteredByType = new ArrayList<Calendar>();
		for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots())
		{
			Integer i = entry.getScheduleCode();
			char c = (char) (i.intValue());

			if (allowableTimeCodes.contains(c))
			{
				allowedTimesFilteredByType.add(entry.getDate());
			}
		}
		return allowedTimesFilteredByType;
	}

	/**
	 * @param index is the entry in allowedTimesFilteredByExistingAppointments we are checking
	 * @param duration
	 * @param allowedTimesFilteredByExistingAppointments
	 * @return
	 */
	private static boolean hasEnoughContiguousTime(int index, long requiredDurationMs, ArrayList<Calendar> allowedTimesFilteredByExistingAppointments, long timeSlotDurationMs)
	{
		// algorithm
		//----------
		// slots         |-A-|-B-|-C-|-D-|-E-|
		// desired           |----X----|
		//
		// given a desired time frame X,
		// we must check each subsequent time slot until endTime<=timeSlot.startTime (B,C,D)
		// must check that each subsequent time slot must start when the previous one ended (as there can be gaps in the list from existing appointments or lunch break etc).

		Calendar startTime = allowedTimesFilteredByExistingAppointments.get(index);
		long endRequiredTimeMs = startTime.getTimeInMillis() + requiredDurationMs;

		// we start processing from that time onwards, no point in looking at earlier time slots
		for (int i = index; i < allowedTimesFilteredByExistingAppointments.size(); i++)
		{
			Calendar timeSlotStart = allowedTimesFilteredByExistingAppointments.get(i);

			// if it ends in the time slot we're inspecting then we've got enough time
			long endTimeSlotMs = timeSlotStart.getTimeInMillis() + timeSlotDurationMs;
			if (endRequiredTimeMs <= endTimeSlotMs) return(true);

			// check if we've hit the end of the list of slots
			if (i + 1 >= allowedTimesFilteredByExistingAppointments.size()) return(false);

			// if the next time slot doesn't follow from the current ones end time, then there's a gap and it's no good.
			Calendar nextTimeSlotStart = allowedTimesFilteredByExistingAppointments.get(i + 1);
			long nextTimeSlotStartMs = nextTimeSlotStart.getTimeInMillis();
			if (endTimeSlotMs != nextTimeSlotStartMs) return(false);
		}

		return false;
	}

	private static boolean isThisTakenByExistingAppointment(Calendar timeSlotStartTime, long durationRequiredMs, List<AppointmentTransfer> existingAppointments)
	{
		long timeSlotStartTimeMs = timeSlotStartTime.getTimeInMillis();
		long timeSlotEndTimeMs = timeSlotStartTimeMs + durationRequiredMs;

		// check if there's already an appointment in this time
		for (AppointmentTransfer appointment : existingAppointments)
		{
			if (appointment.getAppointmentStartDateTime() == null || appointment.getAppointmentEndDateTime() == null)
			{
				logger.error("Appointment is missing timeframe, can't sort out availability. " + getDebugString(appointment));
				return(true);
			}

			long appointmentStart = appointment.getAppointmentStartDateTime().getTimeInMillis();
			long appointmentEnd = appointment.getAppointmentEndDateTime().getTimeInMillis();
			boolean collide = true;

			// cases
			// -------------------------------------------------
			//             |--EXISINTG_APPOINTMENT--|
			// good    |---|
			// bad       |---|
			// bad                   |---|
			// bad                                |---|
			// good                                 |---|
			// bad    |----------------------------------|

			// if this timeslot ends before the appointment starts we're ok
			if (timeSlotEndTimeMs <= appointmentStart) collide = false;

			// if this timeslot starts after the appointment ends we're ok
			if (timeSlotStartTimeMs >= appointmentEnd) collide = false;

			if (collide) return(true);
		}

		return(false);
	}

	public static String getDayOfWeekString(Locale locale, Calendar date)
	{
		int temp = date.get(Calendar.DAY_OF_WEEK);
		DateFormatSymbols symbols = DateFormatSymbols.getInstance(locale);
		return(symbols.getWeekdays()[temp]);
	}

	public static String getTimeDisplayHtmlEscaped(Locale locale, Calendar date)
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", locale);
		return(simpleDateFormat.format(date.getTime()));
	}
}
