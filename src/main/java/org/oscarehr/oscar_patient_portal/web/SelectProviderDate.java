package org.oscarehr.oscar_patient_portal.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.oscarehr.myoscar_server.ws.PersonTransfer2;
import org.oscarehr.oscar_patient_portal.manager.AppointmentType;
import org.oscarehr.oscar_patient_portal.manager.OscarDemographicManager;
import org.oscarehr.oscar_patient_portal.manager.OscarProviderManager;
import org.oscarehr.oscar_patient_portal.manager.OscarScheduleManager;
import org.oscarehr.oscar_patient_portal.utils.MyOscarLoggedInInfo;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;

public final class SelectProviderDate
{
	private static final int MAX_DAYS_TO_SEARCH = 62;
	public static final Comparator<AppointmentType> APPOINTMENT_TYPE_COMPARATOR = new Comparator<AppointmentType>()
	{
		@Override
		public int compare(AppointmentType arg0, AppointmentType arg1)
		{
			if ("other".equalsIgnoreCase(arg0.getName())) return(1);
			if ("other".equalsIgnoreCase(arg1.getName())) return(-1);

			return(arg0.getName().compareTo(arg1.getName()));
		}
	};

	public static List<ProviderTransfer> getCircleOfCareProviders(MyOscarLoggedInInfo loggedInInfo) throws org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception, NotAuthorisedException_Exception
	{
		PersonTransfer2 myOscarPersonTransfer = loggedInInfo.getLoggedInPerson();
		DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByMyOscarUserName(myOscarPersonTransfer.getUserName());
		String patientsOscarProviderNo = oscarDemographic.getProviderNo();

		ArrayList<ProviderTransfer> results = new ArrayList<ProviderTransfer>();

		List<String> team = OscarProviderManager.getAllowedProvidersAndTeam().get(patientsOscarProviderNo);
		if (team != null)
		{
			ProviderTransfer mainProvider = OscarProviderManager.getProvider(patientsOscarProviderNo);
			// status==1 is active
			if (mainProvider != null && "1".equals(mainProvider.getStatus())) results.add(mainProvider);
			for (String tempTeamProviderNo : team)
			{
				ProviderTransfer tempProvider = OscarProviderManager.getProvider(tempTeamProviderNo);
				if (tempProvider != null && "1".equals(tempProvider.getStatus())) results.add(tempProvider);
			}
		}

		return(results);
	}

	public static String getProviderFullNameHtmlEscaped(ProviderTransfer providerTransfer)
	{
		String fullName = providerTransfer.getLastName() + ", " + providerTransfer.getFirstName();
		if (providerTransfer.getSpecialty() != null) fullName = fullName + " (" + providerTransfer.getSpecialty() + ')';
		return(StringEscapeUtils.escapeHtml(fullName));
	}

	public static List<AppointmentType> getAppointmentTypes()
	{
		List<AppointmentType> appointmentTypes = OscarScheduleManager.getAppointmentTypes();
		Collections.sort(appointmentTypes, APPOINTMENT_TYPE_COMPARATOR);
		return(appointmentTypes);
	}

	/**
	 * find next available appointment day excluding/after the day passed in.
	 */
	public static String getNextAvailableAppointmentDateString(String providerId, Long appointmentTypeId, String startDateString) throws NotAuthorisedException_Exception, ParseException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
		Date tempDate = sdf.parse(startDateString);
		GregorianCalendar startCal = new GregorianCalendar();
		startCal.setTime(tempDate);

		// only search up to 2 months in advance
		// start at 1 to start searching from next day so we can just keep calling this for the next day
		for (int i = 1; i < MAX_DAYS_TO_SEARCH; i++)
		{
			Calendar cal = (Calendar) startCal.clone();
			cal.add(Calendar.DAY_OF_YEAR, i);
			cal.getTimeInMillis();

			ArrayList<Calendar> availableSlots = ViewDayAppointmentSlots.getTimeSlots(providerId, appointmentTypeId, cal);
			if (availableSlots.size() > 0) return(DateFormatUtils.ISO_DATE_FORMAT.format(cal));
		}

		return(null);
	}
}
