package org.oscarehr.oscar_patient_portal.web;

import java.util.ArrayList;
import java.util.Calendar;

import org.oscarehr.ws.DayWorkScheduleTransfer;

public interface AvailableTimeSlotFilter
{
	/**
	 * @return a list of available time slots, i.e. make a new array and copy the qualifying time slots into it. Probably best not to alter the passed in list contents.
	 */
	public ArrayList<Calendar> filterAvailableTimeSlots(String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, ArrayList<Calendar> currentlyAllowedTimeSlots);
}
