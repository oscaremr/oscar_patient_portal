package org.oscarehr.oscar_patient_portal.utils;

import java.io.Serializable;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.oscarehr.myoscar.client.ws_manager.AccountManager;
import org.oscarehr.myoscar.client.ws_manager.MyOscarServerCredentials;
import org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception;
import org.oscarehr.myoscar_server.ws.PersonTransfer2;
import org.oscarehr.util.ConfigXmlUtils;

public final class MyOscarLoggedInInfo implements Serializable, MyOscarServerCredentials
{
	private static String myOscarServerBaseUrl = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "myoscar_server_base_url");
	private static final String MY_OSCAR_LOGGED_IN_INFO_SESSION_KEY = "MY_OSCAR_LOGGED_IN_INFO_SESSION_KEY";

	private Long loggedInPersonId = null;
	private String loggedInPersonSecurityToken;
	private String loggedInSessionId;
	private Locale locale;

	public MyOscarLoggedInInfo(Long loggedInPersonId, String loggedInPersonSecurityToken, String loggedInSessionId, Locale locale)
	{
		this.loggedInPersonId = loggedInPersonId;
		this.loggedInPersonSecurityToken = loggedInPersonSecurityToken;
		this.loggedInSessionId = loggedInSessionId;
		this.locale = locale;
	}

	public PersonTransfer2 getLoggedInPerson() throws NotAuthorisedException_Exception
	{
		return(AccountManager.getPerson(this, loggedInPersonId));
	}

	@Override
	public String getLoggedInPersonSecurityToken()
	{
		return(loggedInPersonSecurityToken);
	}

	public void setLoggedInPersonSecurityToken(String loggedInPersonSecurityToken)
	{
		this.loggedInPersonSecurityToken = loggedInPersonSecurityToken;
	}

	@Override
	public Long getLoggedInPersonId()
	{
		return(loggedInPersonId);
	}

	public void setLoggedInPersonId(Long loggedInPersonId)
	{
		this.loggedInPersonId = loggedInPersonId;
	}

	@Override
	public String getLoggedInSessionId()
	{
		return(loggedInSessionId);
	}

	public void setLoggedInSessionId(String loggedInSessionId)
	{
		this.loggedInSessionId = loggedInSessionId;
	}

	public boolean isLoggedIn()
	{
		return(loggedInPersonId != null && loggedInPersonSecurityToken != null);
	}

	public Locale getLocale()
	{
		return(locale);
	}

	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}

	@Override
	public String toString()
	{
		return("loggedInPersonId=" + loggedInPersonId);
	}

	public static MyOscarLoggedInInfo getLoggedInInfo(HttpSession session)
	{
		return (MyOscarLoggedInInfo) (session.getAttribute(MY_OSCAR_LOGGED_IN_INFO_SESSION_KEY));
	}

	public static void setLoggedInInfo(HttpSession session, MyOscarLoggedInInfo loggedInInfo)
	{
		session.setAttribute(MY_OSCAR_LOGGED_IN_INFO_SESSION_KEY, loggedInInfo);
	}

	@Override
	public String getServerBaseUrl()
	{
		return(myOscarServerBaseUrl);
	}

	/**
	 * This is duplicate of getServerBaseUrl() but is required because this one is static while the other implements an interface
	 * @return
	 */
	public static String getMyOscarServerBaseUrl()
	{
		return(myOscarServerBaseUrl);
	}
}