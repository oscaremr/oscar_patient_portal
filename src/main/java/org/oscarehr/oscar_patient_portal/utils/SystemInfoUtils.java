package org.oscarehr.oscar_patient_portal.utils;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.oscarehr.myoscar.client.ws_manager.MyOscarServerWebServicesManager;
import org.oscarehr.myoscar.util.MiscUtils;
import org.oscarehr.myoscar_server.ws.SystemInfoWs;

/**
 * This class is meant as a helper to print information to html pages like the index page etc.
 */
public final class SystemInfoUtils
{
	private static Logger logger = MiscUtils.getLogger();

	private static long serverDataModelVersionRetrieveTime = 0;
	private static int dataModelVersion = 0;

	public static String getBuildDate()
	{
		return(MiscUtils.getBuildDateTime());
	}

	public static String getServerUrl()
	{
		return(MyOscarLoggedInInfo.getMyOscarServerBaseUrl());
	}

	public static String getServerDataModelVersion()
	{
		try
		{
			if (System.currentTimeMillis() - serverDataModelVersionRetrieveTime > DateUtils.MILLIS_PER_HOUR)
			{
				SystemInfoWs systemInfoWs = MyOscarServerWebServicesManager.getSystemPropertiesWs(MyOscarLoggedInInfo.getMyOscarServerBaseUrl());
				dataModelVersion = systemInfoWs.getMyOscarDataModelVersion();
				serverDataModelVersionRetrieveTime = System.currentTimeMillis();
			}

			return(String.valueOf(dataModelVersion));
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			return("Error contacting server.");
		}
	}
}
