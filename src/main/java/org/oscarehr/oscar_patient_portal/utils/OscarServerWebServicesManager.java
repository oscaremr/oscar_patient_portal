package org.oscarehr.oscar_patient_portal.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.oscarehr.myoscar.util.CxfClientUtils;
import org.oscarehr.myoscar.util.MiscUtils;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.DemographicWsService;
import org.oscarehr.ws.FacilityWs;
import org.oscarehr.ws.FacilityWsService;
import org.oscarehr.ws.LoginResultTransfer;
import org.oscarehr.ws.LoginWs;
import org.oscarehr.ws.LoginWsService;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderWs;
import org.oscarehr.ws.ProviderWsService;
import org.oscarehr.ws.ScheduleWs;
import org.oscarehr.ws.ScheduleWsService;
import org.oscarehr.ws.SystemInfoWs;
import org.oscarehr.ws.SystemInfoWsService;

public class OscarServerWebServicesManager
{
	private static Logger logger = MiscUtils.getLogger();
	private static String oscarServerBaseUrl = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "oscar_server_base_url");
	public static String oscarServerUserName = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "oscar_server_user");
	private static String oscarServerPassword = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "oscar_server_password");
	private static LoginResultTransfer loginResultTransfer = null;

	private static void initLoginResultTransfer() throws NotAuthorisedException_Exception
	{
		LoginWs loginWs = getLoginWs();
		LoginResultTransfer temp = loginWs.login(oscarServerUserName, oscarServerPassword);
		if (temp != null)
		{
			loginResultTransfer = temp;
		}
	}

	public static String getMyoscarServerBaseUrl()
	{
		return(oscarServerBaseUrl);
	}

	private static URL buildURL(String servicePoint)
	{
		String urlString = oscarServerBaseUrl + '/' + servicePoint + "?wsdl";

		logger.debug(urlString);

		try
		{
			return(new URL(urlString));
		}
		catch (MalformedURLException e)
		{
			logger.error("Invalid Url : " + urlString, e);
			return(null);
		}
	}

	public static SystemInfoWs getSystemPropertiesWs()
	{
		SystemInfoWsService service = new SystemInfoWsService(buildURL("SystemInfoService"));
		SystemInfoWs port = service.getSystemInfoWsPort();

		CxfClientUtils.configureClientConnection(port);

		return(port);
	}

	public static LoginWs getLoginWs()
	{
		LoginWsService service = new LoginWsService(buildURL("LoginService"));
		LoginWs port = service.getLoginWsPort();

		CxfClientUtils.configureClientConnection(port);

		return(port);
	}

	public static ScheduleWs getScheduleWs() throws NotAuthorisedException_Exception
	{
		if (loginResultTransfer == null) initLoginResultTransfer();

		ScheduleWsService service = new ScheduleWsService(buildURL("ScheduleService"));
		ScheduleWs port = service.getScheduleWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static ProviderWs getProviderWs() throws NotAuthorisedException_Exception
	{
		if (loginResultTransfer == null) initLoginResultTransfer();

		ProviderWsService service = new ProviderWsService(buildURL("ProviderService"));
		ProviderWs port = service.getProviderWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static DemographicWs getDemographicWs() throws NotAuthorisedException_Exception
	{
		if (loginResultTransfer == null) initLoginResultTransfer();

		DemographicWsService service = new DemographicWsService(buildURL("DemographicService"));
		DemographicWs port = service.getDemographicWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static FacilityWs getFacilityWs() throws NotAuthorisedException_Exception
	{
		if (loginResultTransfer == null) initLoginResultTransfer();

		FacilityWsService service = new FacilityWsService(buildURL("FacilityService"));
		FacilityWs port = service.getFacilityWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}
}
