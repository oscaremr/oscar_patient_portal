package org.oscarehr.oscar_patient_portal.utils;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.oscarehr.myoscar.client.ws_manager.AccountManager;
import org.oscarehr.myoscar.client.ws_manager.MyOscarServerWebServicesManager;
import org.oscarehr.myoscar.util.MiscUtils;
import org.oscarehr.myoscar_server.ws.LoginResultTransfer2;
import org.oscarehr.myoscar_server.ws.NotAuthorisedException_Exception;
import org.oscarehr.myoscar_server.ws.RemoteComponentTransfer;
import org.oscarehr.myoscar_server.ws.RemoteComponentTransferText;
import org.oscarehr.myoscar_server.ws.RemoteComponentWs;
import org.oscarehr.oscar_patient_portal.manager.OscarFacilityManager;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.ws.FacilityTransfer;

/**
 * Thread that runs every once in a while
 */
public class RegistrationThread
{
	private static Logger logger = MiscUtils.getLogger();

	private static long reRegisterPeriod = 30000;
	private static String registrationUser;
	private static String registrationPassword;
	private static String entryUri;

	public static class RegistraionTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			logger.debug("Task starting.");

			try
			{
				RegistrationThread.register();
			}
			catch (WebServiceException e)
			{
				logger.error("Unexpected error.", e);
			}
			catch (Exception e)
			{
				logger.error("Unexpected error.", e);
			}

			logger.debug("Task completed.");
		}

	}

	private static Timer timer = new Timer(RegistrationThread.class.getName(), true);

	public static synchronized void scheduleRegistration()
	{
		registrationUser = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "myoscar_server_user");
		registrationPassword = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "myoscar_server_password");
		entryUri = ConfigXmlUtils.getPropertyString("oscar_patient_portal", "application_entry_uri");

		RegistraionTimerTask timerTask = new RegistraionTimerTask();
		// this is dynamically changed so we only run it once right now instead of scheduling a fixed period.
		timer.schedule(timerTask, reRegisterPeriod);
	}

	private static void register() throws NotAuthorisedException_Exception, org.oscarehr.ws.NotAuthorisedException_Exception
	{
		try
		{
			logger.debug("Registering component with myoscar.");

			//--- login ---
			LoginResultTransfer2 loginResultTransfer = AccountManager.login(MyOscarLoggedInInfo.getMyOscarServerBaseUrl(), registrationUser, registrationPassword);

			//--- register component ---
			MyOscarLoggedInInfo tempLoggedInInfo = new MyOscarLoggedInInfo(loginResultTransfer.getPerson().getId(), loginResultTransfer.getSecurityTokenKey(), null, null);
			RemoteComponentWs remoteComponentWs = MyOscarServerWebServicesManager.getRemoteComponentWs(tempLoggedInInfo);

			RemoteComponentTransfer remoteComponentTransfer = new RemoteComponentTransfer();
			FacilityTransfer facilityTransfer = OscarFacilityManager.getDefaultFacility();
			remoteComponentTransfer.setComponentName(facilityTransfer.getName());
			remoteComponentTransfer.setComponentUri(entryUri);

			RemoteComponentTransferText remoteComponentTransferText = new RemoteComponentTransferText();
			remoteComponentTransferText.setLocaleString(Locale.CANADA.toString());
			remoteComponentTransferText.setDisplayName(facilityTransfer.getName());
			remoteComponentTransferText.setDisplayDescription("This is the " + facilityTransfer.getName() + " MyOSCAR App");
			remoteComponentTransfer.getTexts().add(remoteComponentTransferText);

			remoteComponentWs.registerRemoteComponent(remoteComponentTransfer);

			//--- get time for next registration ---
			reRegisterPeriod = remoteComponentWs.getRemoteComponentReRegistrationPeriodMs();
		}
		finally
		{
			scheduleRegistration();
		}
	}
}
