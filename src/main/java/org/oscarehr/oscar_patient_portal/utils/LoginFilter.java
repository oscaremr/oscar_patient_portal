package org.oscarehr.oscar_patient_portal.utils;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.oscarehr.myoscar.util.MiscUtils;

@WebFilter(urlPatterns = { "/*" })
public final class LoginFilter implements javax.servlet.Filter
{
	private static final Logger logger = MiscUtils.getLogger();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		// can't think of anything to do right now.
	}

	@Override
	public void doFilter(ServletRequest tmpRequest, ServletResponse tmpResponse, FilterChain chain) throws IOException, ServletException
	{
		try
		{
			HttpServletRequest request = (HttpServletRequest) tmpRequest;
			HttpServletResponse response = (HttpServletResponse) tmpResponse;

			loginIfParametersPassedIn(request);

			HttpSession session = request.getSession();
			MyOscarLoggedInInfo loggedInInfo = MyOscarLoggedInInfo.getLoggedInInfo(session);

			if ((loggedInInfo != null && loggedInInfo.isLoggedIn()) || isExempt(request.getServletPath()))
			{
				chain.doFilter(tmpRequest, tmpResponse);
			}
			else
			{
				logger.warn("Attempted access to resource while not logged in. " + request.getServletPath());
				response.sendRedirect(request.getContextPath() + "/index.jsp");
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			throw(new ServletException(e));
		}
	}

	private void loginIfParametersPassedIn(HttpServletRequest request)
	{
		String tempUserId = request.getParameter("userId");
		String token = request.getParameter("securityToken");

		logger.debug("userId=" + tempUserId + ", securityToken=" + token);

		if (tempUserId != null && token != null)
		{
			Long userId = Long.parseLong(tempUserId);

			request.getSession().invalidate();
			HttpSession session = request.getSession();

			MyOscarLoggedInInfo loggedInInfo = new MyOscarLoggedInInfo(userId, token, session.getId(), request.getLocale());
			MyOscarLoggedInInfo.setLoggedInInfo(session, loggedInInfo);
		}
	}

	@Override
	public void destroy()
	{
		// can't think of anything to do right now.
	}

	public static boolean isExempt(String path)
	{
		if (path.startsWith("/resources/")) return(true);
		else if (path.startsWith("/customisations/")) return(true);
		else if (path.equals("/")) return(true);
		else if (path.equals("/index.jsp")) return(true);
		else if (path.equals("/login.jsp")) return(true);
		else if (path.equals("/login_action.jsp")) return(true);
		else if (path.equals("/logout.jsp")) return(true);
		else if (path.equals("/error500.jsp")) return(true);
		else if (path.endsWith(".css")) return(true);
		else if (path.endsWith(".gif")) return(true);
		else if (path.endsWith(".png")) return(true);
		else if (path.endsWith(".jpg")) return(true);
		else if (path.endsWith(".js")) return(true);
		else if (path.equals("/test_index.jsp")) return(true);
		else if (path.equals("/test_login.jsp")) return(true);
		else return(false);
	}
}
