package org.oscarehr.oscar_patient_portal.utils;

import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.oscarehr.myoscar.util.MiscUtils;

@WebListener
public final class ContextStartupListener implements javax.servlet.ServletContextListener
{
	private static final Logger logger = MiscUtils.getLogger();

	@Override
	public void contextInitialized(javax.servlet.ServletContextEvent sce)
	{
		try
		{
			String contextPath = sce.getServletContext().getContextPath();
			logger.info("Server processes starting. context=" + contextPath);

			MiscUtils.addLoggingOverrideConfiguration(contextPath);

			RegistrationThread.scheduleRegistration();

			logger.info("Server processes starting completed. context=" + contextPath);
		}
		catch (Exception e)
		{
			logger.error("Unexpected Error.", e);
		}
	}

	@Override
	public void contextDestroyed(javax.servlet.ServletContextEvent sce)
	{
		try
		{
			logger.info("Server processes stopping. context=" + sce.getServletContext().getContextPath());
		}
		catch (Exception e)
		{
			logger.error("Unexpected Error.", e);
		}
	}
}
