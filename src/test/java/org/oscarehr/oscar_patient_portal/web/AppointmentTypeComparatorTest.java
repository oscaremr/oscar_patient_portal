package org.oscarehr.oscar_patient_portal.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;
import org.oscarehr.oscar_patient_portal.manager.AppointmentType;

public class AppointmentTypeComparatorTest
{
	@Test
	public void test()
	{
		AppointmentType aaaa = new AppointmentType();
		aaaa.setName("aaaa");

		AppointmentType zzzz = new AppointmentType();
		zzzz.setName("zzzz");

		AppointmentType other = new AppointmentType();
		other.setName("other");

		int result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(aaaa, zzzz);
		assertTrue(result < 0);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(aaaa, aaaa);
		assertEquals(0, result);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(zzzz, aaaa);
		assertTrue(result > 0);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(aaaa, other);
		assertTrue(result < 0);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(zzzz, other);
		assertTrue(result < 0);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(other, aaaa);
		assertTrue(result > 0);

		result = SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR.compare(other, zzzz);
		assertTrue(result > 0);

		ArrayList<AppointmentType> list = new ArrayList<AppointmentType>();
		list.add(zzzz);
		list.add(other);
		list.add(aaaa);
		Collections.sort(list, SelectProviderDate.APPOINTMENT_TYPE_COMPARATOR);
		assertEquals("aaaa", list.get(0).getName());
		assertEquals("zzzz", list.get(1).getName());
		assertEquals("other", list.get(2).getName());
	}
}