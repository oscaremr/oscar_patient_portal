Filters can be applied to available appointment slots.

The initial use case is : "Open Access" appointments must only be allowed to be bookable 24 hours in advance.

So, the regular appointment sorting out goes through it's logic and returns a list of available timeslots, then it continues to run through the OpenAccess24hTimeSlotFilter.java class and removes any open access timeslots not within 24 hours.

To get the above working we have AvailableTimeSlotFilter.java which is a call back interface.

configure/add your filter in config.xml as <available_time_slot_filter_class list_entry="true"> (see the reconfigured OpenAccess example in there).

The appointments your filter return should be a list of "allowed" appointment times. This means to can add / remove appointments as you want.