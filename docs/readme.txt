THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
THIS PROJECT IS DEPRECATED, PLEASE LOOK AT oscar_clinical_component instead.
----------------------------------------------------------------------------

This is the Oscar Patient Portal project.

--------
Overview
--------
The initial intent is to provide a few web pages so a client can book appointments. The initial use case is to redirect patients here from the myoscar client when they want to book appointments.

The longer term intent is to provide a basic web site or portal for a clinic. This could allow a site to provide public information like hours of operation or a list of providers and or services offered at this clinic etc.

-----------------
Technical details
-----------------
This is a maven2 project. To build the code you run "mvn package" and you should find a war file in the target directory.

The projects war file should be a standard war file and should in theory deploy like any other war in any normal servlet container (although tomcat is the only one tested).

From a runtime point of view, the client is just a set of web pages which communicate against the oscar web services as well as against the myoscar web services. No information is stored locally. 

--------------------
Installation details
--------------------
1) build the code or download the war archive
2) deploy/copy the war file into a servlet container like any standard war.
3) When starting tomcat, you can pass a system parameter of "-Doscar_patient_portal_config=override.xml" where override.xml is an xml file which overlays the default one.
4) config.xml (and it's override) The 2 most important settings would be the oscar web services settings and the myoscar server web services settings, they need to be correct for this to connect to them.
   There's also 3 parameters that need to be setup, the list of oscar provider numbers, appointmentTypes, and time codes. These parameters control which providers show up on the booking list, what appointment types they can pick from, and what time slots in the providers schedule is eligible for online booking.
   You will need to have setup oscar with the schedule templates for the providers working schedule. This means you must also have created appointment types and time codes for the schedule, also make sure you setup the holiday days properly in oscar.
5) go to the client url, i.e. http://127.0.0.1:8285/oscar_patient_portal/test_login.jsp that should bring up a test login page to check if everything is working properly, you can by pass the myoscar client by using this page but you must fill in the userId (not username)
6) From the myoscar_client side, assuming your configuration is correct, it should register itself with the myoscar system with in a minute if you starting the server. Once that happens you should see it as an option under the persons preferences.