#!/bin/sh

# This script is used to setup the config to run the server / tomcat.

ulimit -n 2048

export CATALINA_HOME=${APP_ROOT}/apache-tomcat-7.0.14
export PATH=${CATALINA_HOME}/bin:${PATH}

export WORKING_ROOT=`pwd`

export CATALINA_BASE=${WORKING_ROOT}/catalina_base

MEM_SETTINGS="-Xms384m -Xmx384m -Xss128k -XX:MaxNewSize=96m -XX:MaxPermSize=384m -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 "
export JAVA_OPTS="-Djava.awt.headless=true -server -Xincgc -Xshare:off -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -Dlog4j.override.configuration=${WORKING_ROOT}/override_log4j.xml -Doscar_patient_portal_config=override.xml "${MEM_SETTINGS}

